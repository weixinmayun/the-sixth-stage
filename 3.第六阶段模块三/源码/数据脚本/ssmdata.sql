/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.7.28-log : Database - ssmdata
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ssmdata` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ssmdata`;

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `dept_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `dept_name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `major_name` varchar(50) DEFAULT NULL COMMENT '部门主管',
  `telephone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `department` */

insert  into `department`(`dept_id`,`dept_name`,`major_name`,`telephone`,`email`) values 
(1,'市场部','拉钩招聘','400-400-4008','lagou@163.com'),
(2,'销售部','拉钩招聘','400-400-4008','lagou@163.com');

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `emp_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '员工id',
  `emp_name` varchar(50) DEFAULT NULL COMMENT '员工姓名',
  `dept_id` int(20) NOT NULL COMMENT '部门号',
  `job_name` varchar(50) DEFAULT NULL COMMENT '职位',
  `join_date` date DEFAULT NULL COMMENT '入职时间',
  `telephone` varchar(50) DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `employee` */

insert  into `employee`(`emp_id`,`emp_name`,`dept_id`,`job_name`,`join_date`,`telephone`) values 
(1,'小王',1,'客户经理','2020-06-06','1233434334'),
(2,'老于',1,'客户经理','2019-04-27','1233434334'),
(3,'老方',2,'销售经理','2015-08-14','1233434334'),
(4,'老李',1,'大堂经理','2020-12-12',NULL),
(5,'李四',1,'大内密探','2020-12-12',NULL),
(6,'正成功',1,'架构师','2020-03-04','15322612'),
(7,'可失败',2,'高级工程师','2020-03-04','4654645');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
