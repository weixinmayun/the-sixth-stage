<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
    <!-- 指定字符集 -->
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>添加用户</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/jquery-2.1.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            let modulePathName = window.document.location.pathname;
            // 获取模块名称：/module01
            let moduleName = modulePathName.split('/')
            let path=moduleName[0]+"/"+moduleName[1];
            $.get(path+"/department/getDepartmentlist", {
            }, function (data, status) {

                var dataObj=eval(data);//转换为数组对象

                $.each( dataObj, function(i, item){           //遍历数组
                    $("#dropDeptId").append("<option value="+item.dept_id+">"+item.dept_name+"</option>");
                });


            });
            $("#dropDeptId").change(function () {
                $("#dept_id").val($("#dropDeptId option:selected").val());

            });

            $("#btnConfirm").click(function () {
                var modulePathName = window.document.location.pathname;
                // 获取模块名称：/module01
                var moduleName = modulePathName.split('/')
                var path=moduleName[0]+"/"+moduleName[1];

                if($("#emp_name").val()==""){
                    alert("姓名不能为空");return false;
                }
                if($("#job_name").val()==""){
                    alert("职位不能为空");return false;
                }
                if($("#join_date").val()==""){
                    alert("入职日期不能为空");return false;
                }
                if($("#telephone").val()==""){
                    alert("联系方式不能为空");return false;
                }
                if($("#dropDeptId option:selected").val()==0){
                    alert("请选择部门");return false;
                }


            });
        })


    </script>
</head>
<body>
<div class="container">
    <center><h3>添加账户</h3></center>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <form action="${pageContext.request.contextPath}/employee/saveEmployee" method="post">
                <div class="form-group">
                    <label for="emp_name">姓名：</label>
                    <input type="text" class="form-control" id="emp_name" name="emp_name" placeholder="请输入姓名">
                </div>
                <div class="form-group">
                    <label for="job_name">职位：</label>
                    <input type="text" class="form-control" id="job_name" name="job_name" placeholder="请输入职位">
                </div>
                <div class="form-group">
                    <label for="telephone">联系方式：</label>
                    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="请输入联系方式">
                </div>
                <div class="form-group">
                    <label for="join_date">入职日期：</label>
                    <input type="text" class="form-control" id="join_date" name="join_date" placeholder="请输入联系方式">
                </div>

                <div class="form-group">
                    <label for="dropDeptId">部门：</label>
                    <select  id="dropDeptId" class="form-control">
                        <option value="0">--请选择--</option>
                    </select>
                </div>
                <input type="hidden" id="dept_id" name="dept_id">
                <div class="form-group" style="text-align: center">
                    <input class="btn btn-primary" type="submit" value="提交" id="btnConfirm" />
                    <input class="btn btn-default" type="reset" value="重置" />
                    <input class="btn btn-default" type="button" onclick="history.go(-1)" value="返回" />
                </div>

            </form>
        </div>
        <div class="col-lg-3"></div>
    </div>
</div>
</body>
</html>