<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.ParseException" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2020/12/12
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css"  href="<%=request.getContextPath()%>/css/bootstrap.css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.0.min.js" ></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
    <style>
        td,th{
            border:1px solid #000000;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            let modulePathName = window.document.location.pathname;
            // 获取模块名称：/module01
            let moduleName = modulePathName.split('/')
            let path=moduleName[0]+"/"+moduleName[1];
            $.get(path+"/department/getDepartmentlist", {
            }, function (data, status) {

                var dataObj=eval(data);//转换为数组对象

                $.each( dataObj, function(i, item){           //遍历数组
                    $("#dropDeptId").append("<option value="+item.dept_id+">"+item.dept_name+"</option>");
                });


            });
            changeColorCommon();
        })
        function changeColorCommon() {
            $("#tbUserInfo tbody tr:even").css("background-color", "#ffcc99");
            $("#tbUserInfo  tbody tr:odd").css("background-color", "#00ccff");

            $("#btnQuery").click(function () {
                var modulePathName = window.document.location.pathname;
                // 获取模块名称：/module01
                var moduleName = modulePathName.split('/')
                var path=moduleName[0]+"/"+moduleName[1];
                var userName = $("#txtUserName").val();
               if(userName.trim()==""){
                   alert("请输入查询内容");
                   return false;
               }
                window.location.href = path + "/employee/getSearchEmployeeList?emp_name=" + userName;

            });
            $("#txtUserName").focus(function () {
                $("#txtUserName").val("");

            });
            $("#btnrefresh").click(function () {
                let modulePathName = window.document.location.pathname;
                // 获取模块名称：/module01
                let moduleName = modulePathName.split('/')
                let path=moduleName[0]+"/"+moduleName[1];
                window.location.href=path+"/employee/getEmployeeList";
            });
            /*给删除选中按钮绑定点击事件*/
            $('#btnDelete').click(function () {
                var modulePathName = window.document.location.pathname;
                // 获取模块名称：/module01
                var moduleName = modulePathName.split('/')
                var path=moduleName[0]+"/"+moduleName[1];
                if(confirm('您确定要删除吗')){
                    if($('input[name=ids]:checked').length > 0){
                        let strIds="";
                        $('input[name=ids]:checked').each(function () {
                            strIds+=$(this).val()+",";
                        })
                         if(strIds!="") {
                             strIds=strIds.substring(0,strIds.length-1);
                         }
                        alert(strIds);
                        $.get(path+"/employee/deleteEmployeeByList", {
                            ids:strIds
                        }, function (data, status) {
                            alert(data+"----"+status);
                            if (data>1) {
                                window.location.href = path + "/employee/getEmployeeList";
                            }
                            else {
                                alert("删除失败!");
                            }
                        });


                    }

                }else {
                    alert('请选中删除行')
                }


            })
            $("#btnAdd").click(function () {
                let modulePathName = window.document.location.pathname;
                // 获取模块名称：/module01
                let moduleName = modulePathName.split('/')
                let path=moduleName[0]+"/"+moduleName[1];
                window.location.href=path+"/add.jsp";
            })
            $("#dropDeptId").change(function () {
                $("#dept_id").val($("#dropDeptId option:selected").val());

            });

        }

    </script>
</head>
<div style="text-align:center; font-size: 35px;"> 员工信息管理</div>
<hr>
<br>
<div style="margin-left:270px;">
<button class="btn btn-info" id="btnAdd">新增</button>
<button class="btn btn-info" id="btnrefresh">刷新</button><button class="btn btn-info" id="btnQuery">查询</button>
<input type="text" value="按姓名查询" id="txtUserName" />
</div>
<table id="tbUserInfo" style="width:1000px; margin:auto;">
    <thead><th>编号</th><th>姓名</th><th>职位</th><th>入职日期</th><th>联系方式</th> </thead>
    <tbody>

    <c:forEach var="ls" items="${employeelist}">
        <tr><td><input type="checkbox" name="ids" value="${ls.emp_id}" /> </td>
            <td><c:out value="${ls.emp_name}"></c:out></td>
            <td><c:out value="${ls.job_name}"></c:out></td>
            <td>
                <c:out value="${ls.join_date}"></c:out>
               </td>
            <td><c:out value="${ls.telephone}"></c:out></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
