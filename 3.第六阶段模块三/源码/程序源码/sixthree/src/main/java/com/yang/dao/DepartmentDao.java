package com.yang.dao;

import com.yang.domain.Department;

import java.util.List;

public interface DepartmentDao {
    public List<Department> findById();
}
