package com.yang.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yang.domain.Department;
import com.yang.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    //获取部门信息
    @RequestMapping(value = "/getDepartmentlist")
    @ResponseBody
    public void returnVoid(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Department> list=departmentService.findById();
        ObjectMapper mapper = new ObjectMapper();
        String jsonlist = mapper.writeValueAsString(list);

// 1.通过response直接响应数据

 response.setContentType("text/html;charset=utf-8");
 response.getWriter().write(jsonlist);
    }
}
