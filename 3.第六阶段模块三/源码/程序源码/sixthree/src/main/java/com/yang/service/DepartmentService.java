package com.yang.service;

import com.yang.domain.Department;

import java.util.List;

public interface DepartmentService {
    public List<Department> findById();
}
