package com.yang.service.impl;

import com.yang.dao.EmployeeDao;
import com.yang.domain.Employee;
import com.yang.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeDao employeeDao;
   @Override
    public List<Employee> getEmployeeList() {
        return employeeDao.getEmployeeList();
    }

    @Override
    public List<Employee> getSearchEmployeeList(String emp_name) {
        return employeeDao.getSearchEmployeeList(emp_name);
    }
    @Override
    public int addEmployee(Employee employee) {
        return employeeDao.addEmployee(employee);
    }
    @Override
    public int updateEmployee(Employee employee) {
        return  employeeDao.updateEmployee(employee);
    }
    @Override
    public int deleteEmployeeByList(Integer[] ids) {
        return employeeDao.deleteEmployeeByList(ids);
    }
}
