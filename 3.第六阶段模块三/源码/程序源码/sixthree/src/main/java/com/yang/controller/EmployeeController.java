package com.yang.controller;

import com.yang.converter.DateConverter;
import com.yang.domain.Employee;
import com.yang.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    //查询员工列表
    @RequestMapping("/getEmployeeList")
    public String getEmployeeList(Model model){
        List<Employee> list= employeeService.getEmployeeList();
        // 把封装好的list存到model中
        model.addAttribute("employeelist",list);
        return "employeelist";
    }
    //添加员工
   @RequestMapping("/saveEmployee")
    public  String saveEmployee(Employee employee){

            int flag = employeeService.addEmployee(employee);
            if (flag < 1) {
                return "erro";
            }
       // 跳转到getEmployeeList方法从新查询一次数据库进行数据的遍历展示
       return "redirect:/employee/getEmployeeList";
    }
    //通过员工姓名查询员工列表
    @RequestMapping("/getSearchEmployeeList")
    public String getSearchEmployeeList(String emp_name,Model model){
        List<Employee> list= employeeService.getSearchEmployeeList("%"+emp_name+"%");
        // 把封装好的list存到model中
        model.addAttribute("employeelist",list);
        return "employeelist";
    }
    //删除员工(未完)
    @RequestMapping("/deleteEmployeeByList")
    public String deleteEmployeeByList(String ids) {
        if(ids!="") {
            Integer [] num=new Integer[ids.toCharArray().length];

            int[] test={};
      int flag=  employeeService.deleteEmployeeByList(num);
      if(flag>0) {
          return "redirect:/employee/getEmployeeList";
      }
        }
       return "redirect:/erro.jsp";
    }

}
