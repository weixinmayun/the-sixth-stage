package com.yang.domain;

import java.io.Serializable;

public class Department  implements Serializable {
    private int dept_id;//部门id
    private String dept_name;//部门名称
    private String major_name;//部门主管
    private String telephone;//练习方式
    private String email;//邮件

    @Override
    public String toString() {
        return "Department{" +
                "dept_id=" + dept_id +
                ", dept_name='" + dept_name + '\'' +
                ", major_name='" + major_name + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getMajor_name() {
        return major_name;
    }

    public void setMajor_name(String major_name) {
        this.major_name = major_name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Department(int dept_id, String dept_name, String major_name, String telephone, String email) {
        this.dept_id = dept_id;
        this.dept_name = dept_name;
        this.major_name = major_name;
        this.telephone = telephone;
        this.email = email;
    }

    public Department() {
    }
}
