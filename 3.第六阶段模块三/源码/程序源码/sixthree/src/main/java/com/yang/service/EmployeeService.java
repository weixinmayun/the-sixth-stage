package com.yang.service;

import com.yang.dao.EmployeeDao;
import com.yang.domain.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeService {
    public List<Employee> getEmployeeList();
    public List<Employee> getSearchEmployeeList(String emp_name);
    public  int addEmployee(Employee employee);
    public  int updateEmployee(Employee employee);
    public  int deleteEmployeeByList(Integer[] ids);
}
