package com.yang.domain;

import java.io.Serializable;
import java.util.Date;

public class Employee implements Serializable {
    private int emp_id;//员工编号
    private String   emp_name;//员工姓名
    private  Department department ;//部门信息
    private  String job_name;//职位
    private Date join_date;//入职日期
    private  String telephone;//练习方式
    private  int dept_id;

    @Override
    public String toString() {
        return "Employee{" +
                "emp_id=" + emp_id +
                ", emp_name='" + emp_name + '\'' +
                ", department=" + department +
                ", job_name='" + job_name + '\'' +
                ", join_date=" + join_date +
                ", telephone='" + telephone + '\'' +
                ", dept_id=" + dept_id +
                '}';
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public Date getJoin_date() {
        return join_date;
    }

    public void setJoin_date(Date join_date) {
        this.join_date = join_date;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public Employee(int emp_id, String emp_name, Department department, String job_name, Date join_date, String telephone, int dept_id) {
        this.emp_id = emp_id;
        this.emp_name = emp_name;
        this.department = department;
        this.job_name = job_name;
        this.join_date = join_date;
        this.telephone = telephone;
        this.dept_id = dept_id;
    }

    public Employee() {
    }
}
