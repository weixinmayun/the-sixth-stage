package com.yang.service.impl;

import com.yang.dao.DepartmentDao;
import com.yang.domain.Department;
import com.yang.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl  implements DepartmentService {
    @Autowired
    DepartmentDao departmentDao;
    @Override
    public List<Department> findById() {
        return departmentDao.findById();
    }
}
