package com.yang.dao;

import com.yang.domain.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeDao {
    public List<Employee> getEmployeeList();
    public  int addEmployee(Employee employee);
    public  int updateEmployee(Employee employee);
    public List<Employee> getSearchEmployeeList(String emp_name);
    public  int deleteEmployeeByList(Integer[] ids);
}
