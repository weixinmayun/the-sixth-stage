package com.yang.domain;

import java.io.Serializable;

public class TComment implements Serializable {
    @Override
    public String toString() {
        return "TComment{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", author='" + author + '\'' +
                ", a_id=" + a_id +
                '}';
    }

    public int id;
    public String content;
    public String author;
    public  int  a_id;
    public TComment() {
    }
    public TComment(int id, String content, String author, int a_id) {
        this.id = id;
        this.content = content;
        this.author = author;
        this.a_id = a_id;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getA_id() {
        return a_id;
    }

    public void setA_id(int a_id) {
        this.a_id = a_id;
    }






}
