package com.yang.mapper;

import com.yang.domain.TComment;

import java.util.List;

public interface TCommentMap {

    public List<TComment> getCommentById(int id);
}
