package com.yang.test;


import com.yang.domain.TArticle;
import com.yang.mapper.TArticleMap;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisTest {
    @Test
    public void test1() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        TArticleMap mapper = sqlSession.getMapper(TArticleMap.class);

         List<TArticle> tComments = mapper.getList();

        for (TArticle comment : tComments) {
            System.out.println(comment);
            System.out.println(comment.gettCommentList());
        }

        sqlSession.close();
    }
}
