package com.yang.domain;

import java.io.Serializable;
import java.util.List;

public class TArticle implements Serializable {
    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<TComment> gettCommentList() {
        return tCommentList;
    }

    public void settCommentList(List<TComment> tCommentList) {
        this.tCommentList = tCommentList;
    }

    public String title;
    public String content;
    public  List<TComment> tCommentList;
    @Override
    public String toString() {
        return "TArticle{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", tCommentList=" + tCommentList +
                '}';
    }


    public TArticle() {
    }
    public TArticle(int id, String title, String content, List<TComment> tCommentList) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tCommentList = tCommentList;
    }









}
