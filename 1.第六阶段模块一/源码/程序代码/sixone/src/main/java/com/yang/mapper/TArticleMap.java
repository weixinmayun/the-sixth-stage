package com.yang.mapper;

import com.yang.domain.TArticle;
import com.yang.domain.TComment;

import java.util.List;

public interface TArticleMap {

    public  List<TArticle> getList();

}
