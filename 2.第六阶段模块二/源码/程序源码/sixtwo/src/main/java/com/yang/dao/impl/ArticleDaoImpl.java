package com.yang.dao.impl;

import com.yang.dao.ArticleDao;
import com.yang.domain.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleDaoImpl implements ArticleDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert(Article article) {
     String sql="insert into t_article values(null,?,?)";
     int update = jdbcTemplate.update(sql,article.getTitle(),article.getContent());
     System.out.println("添加后返回的状态"+update);

    }
}
