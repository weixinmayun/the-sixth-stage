package com.yang.test;

import com.yang.domain.Article;
import com.yang.service.ArticleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class ArticleTest {
    @Autowired
    private ArticleService articleService;
    @Test
    public  void testInsert(){
        Article article=new Article();
        article.setTitle("Java从入门到放弃");
        article.setContent("我是不会放弃滴!");
        articleService.insert(article);
    }
}
