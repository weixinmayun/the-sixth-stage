package com.lagou.controller;

import com.lagou.domain.ResourceCategory;
import com.lagou.domain.ResponseResult;
import com.lagou.service.ResourceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ResourceCategory")
public class ResourceCategoryController {

    @Autowired
    private ResourceCategoryService resourceCategoryService;

    /**
     * 查询所有分类信息
     * @return
     */
    @RequestMapping("/findAllResourceCategory")
    public ResponseResult findAllResourceCategory(){
        List<ResourceCategory> allResourceCategory = resourceCategoryService.findAllResourceCategory();
        return  new ResponseResult(true,200,"查询所有分类信息成功",allResourceCategory);
    }

    /**
     * 新增资源分类或修改分类
     * @param resourceCategory
     * @return
     */
    @RequestMapping("/saveResourceCategory")
    public  ResponseResult saveResourceCategory(@RequestBody ResourceCategory resourceCategory){
        if(resourceCategory.getId()==null){
            resourceCategoryService.saveResourceCategory(resourceCategory);
            return  new ResponseResult(true,200,"新增资源分类成功",null);
        }
        else {
            resourceCategoryService.updateResourceCategory(resourceCategory);
            return  new ResponseResult(true,200,"修改资源分类成功",null);
        }

    }

    /**
     * 删除资源分类成功
     * @param id
     * @return
     */
    @RequestMapping("/deleteResourceCategory")
    public  ResponseResult deleteResourceCategory(Integer id){
        resourceCategoryService.deleteResourceCategory(id);
        ResponseResult responseResult = new ResponseResult(true, 200, "删除资源分类成功", null);
        return  responseResult;
    }
}
